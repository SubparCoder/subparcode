"""@file Lab1.py
@brief Vendotron Program 
@details Program initializes and prompts you to insert money. It then takes
         your money and adds it to your balance while keeping track of how 
         many of each denomination you have inserted. Once you choose a drink
         it will decide if you inserted enough money. If not, it will tell
         you that you have insufficient funds. If you do, it will subtract the
         cost of your drink and spit out your change in the form of how many
         of each denomination you are getting back, and tells you that it is
         dispensing your drink. At any point you can press eject and it will 
         tell you that it is giving back your money and the the balance will 
         clear and it will once again prompt you to insert money.
@author Jacob Everest
@date Jan. 14, 2021
@copyright I dont even know what that word means
"""

##@brief State flag
# @details Changes depending on where vendotron needs to go next
state = 0
import keyboard   #import keyboard library
##@brief Pushed key storage
# @details where the key that was pushed
pushed_key = None
##@brief Your bankroll
# @details How much money you've put in
your_money = 0
##@brief Drink cost
# @details Storage for the cost of the drink that you selected
cost = 0
##@brief Your bills variable
# @details The bills that will be used to purchase (your money in denom version)
your_bills = 0
##@brief Your change
# @details the amount of change you will get back after purchase
new_change = 0
##@brief Drink storage
# @details Stores the name of drink for later dispensing
drink = 0
##@brief problem ejecting
# @details If nothing was inserted and you want money
eject_error = 0
##@brief Ejected change value
# @details Amount of change to be ejected after transaction
eject_change = 0


def on_keypress (thing):
    """ 
    @brief Callback which runs when the user presses a key.
    @details Professor Ridgely gave us this and frankly I dont understand it
    @param thing it is a thing
    @author Jacob Everest
    @date Jan. 14, 2021
    """
    global pushed_key #I don't understand this function

    pushed_key = thing.name
keyboard.on_press (on_keypress)
    
def getChange(price, payment):
    """
    @brief Computes change
    @details Takes the payment denominations, multiplies them out to find their
             value, subtracts the value of the cost, and then does some
             division magic to determine the change denominations.
    @param price the cost of the drink
    @param payment the value the user has entered
    @author Jacob Everest
    @date Jan. 14, 2021
    """
    change_holder = 0
    twenties = 0
    tens = 0
    fives = 0
    ones = 0
    quarters = 0
    dimes = 0
    nickels = 0
    pennies = 0
    oh_no = 0

    #multiply out the value of the payment and then subtract the price
    change_holder= round(((payment[0]*0.01)+(payment[1]*0.05)+(payment[2]*0.1)+
               (payment[3]*0.25)+(payment[4]*1)+(payment[5]*5)+
               (payment[6]*10)+(payment[7]*20)), 2) - price
    
    if change_holder < 0: #if payment wasn't high enough
        oh_no = 1
    
    twenties = round(change_holder, 2)//20 #find how many twenties
    change_holder = round(change_holder, 2) % 20 #take whats left
    tens = round(change_holder, 2)//10     #repeat and round
    change_holder = round(change_holder, 2) % 10
    fives = round(change_holder, 2)//5
    change_holder = round(change_holder, 2) % 5
    ones = round(change_holder, 2)//1
    change_holder = change_holder % 1
    quarters = round(change_holder, 2)//0.25
    change_holder = round(change_holder, 2) % 0.25
    dimes = round(change_holder, 2)//0.1
    change_holder = round(change_holder, 2) % 0.1
    nickels = round(change_holder, 2)//0.05
    change_holder = round(change_holder, 2) % 0.05
    pennies = round(change_holder, 2)//0.01

    change = (pennies, nickels, dimes, quarters, ones, fives,
              tens, twenties) #place in change
    return [oh_no, change]

def printWelcome():           #Print message from the rooskies
    """
    @brief Prints welcome message and states prices
    @details Its very simple. It prints a message. Then it state the price of 
             each drink.
    @param nothing runs automatically
    @author Jacob Everest
    @date Jan. 14, 2021
    """
    print('Eensort DeeNomeenations Comrade')
    print('Cuke = $2.35')
    print('Popsi = $0.25 because eets terrible')
    print('Spryte = $0.85')
    print('Dr. Pupper = $1.25')


while True:
    """Implement FSM using a while loop and an if statement
    will run eternally until user presses CTRL-C
    """

    if state == 0:         #First state just waiting
        """perform state 0 operations
        this init state, initializes the FSM itself, with all the
        code features already set up
        """
        your_money = 0
        penny = 0
        nickel = 0
        dime = 0
        quarter = 0
        one = 0
        five = 0
        ten = 0
        twenty = 0
        pushed_key = None
        printWelcome()
        state = 1 # on the next iteration, the FSM will run state 1
    
    elif state == 1:        #button pressed, come here now, check if we care
        """
        Wait for money to be inserted.
        """
        if pushed_key:
            if pushed_key == '0':
                state = 2
            elif pushed_key == '1':
                state = 2
            elif pushed_key == '2':
                state = 2
            elif pushed_key == '3':
                state = 2
            elif pushed_key == '4':
                state = 2
            elif pushed_key == '5':
                state = 2
            elif pushed_key == '6':
                state = 2
            elif pushed_key == '7':
                state = 2
            else:
                pass
        else:
            state = 1 
    
    elif state == 2: #checks what money was inserted
        """
        Counts up the money and states it as your balance. If you press a drink
        it will send you to state 3, if you add more money it keeps you in
        state 2, and if you press eject it will clear your balance and send you
        back to state 0. Anything else and it will ignore you.
        """
        if pushed_key == '0': 
            pushed_key = None
            your_money = round(your_money + 0.01, 2) #add money and round
            penny = penny + 1 #increment how many pennies added (repeat)
            print('Balance = ${}'.format (your_money))
            state = 2
        elif pushed_key == '1':
            pushed_key = None
            your_money = round(your_money + 0.05, 2)
            nickel = nickel + 1
            print('Balance = ${}'.format (your_money))
            state = 2
        elif pushed_key == '2':
            pushed_key = None
            your_money = round(your_money + 0.1, 2)
            dime = dime + 1
            print('Balance = ${}'.format (your_money))
            state = 2
        elif pushed_key == '3':
            pushed_key = None
            your_money = round(your_money + 0.25, 2)
            quarter = quarter + 1
            print('Balance = ${}'.format (your_money))
            state = 2
        elif pushed_key == '4':
            pushed_key = None
            your_money = your_money + 1
            one = one + 1
            print('Balance = ${}'.format (your_money))
            state = 2
        elif pushed_key == '5':
            pushed_key = None
            your_money = your_money + 5
            five = five + 1
            print('Balance = ${}'.format (your_money))
            state = 2
        elif pushed_key == '6':
            pushed_key = None
            your_money = your_money + 10
            ten = ten + 1
            print('Balance = ${}'.format (your_money))
            state = 2
        elif pushed_key == '7':
            pushed_key = None
            your_money = your_money + 20
            twenty = twenty + 1
            print('Balance = ${}'.format (your_money))
            state = 2
        elif pushed_key == 'e': #eject but also simplify how much is dispensed
            pushed_key = None  #if you inserted five ones you get one five
            [eject_error, eject_change] = getChange(0, [penny, nickel, dime,
                                                        quarter, one, five, 
                                                        ten, twenty])
            print('Dispensing ${}'.format (eject_change))
            your_money = 0
            state = 0
        elif pushed_key == 'c':
            pushed_key = None
            cost = 2.35
            drink = 'Cuke'
            state = 3 # s2 -> s3
        elif pushed_key == 'p':
            pushed_key = None
            cost = 0.25 #because pepsi is garbage, even with an o in the name
            drink = 'Popsi'
            state = 3 # s2 -> s3
        elif pushed_key == 's':
            pushed_key = None
            cost = 0.85
            drink = 'Spryte'
            state = 3 # s2 -> s3
        elif pushed_key == 'd':
            pushed_key = None
            cost = 1.25
            drink = 'Dr. Pupper'
            state = 3 # s2 -> s3
        else:
            state = 2
    
    elif state == 3:  #get your change and the drink
        """
        Puts your bills into a tuple, sends that into the getChange function 
        with the cost, and recieves the status of an error flag and the change
        you are getting back
        """
        your_bills = (penny, nickel, dime, quarter, one, five, ten, twenty)
        [oh_no, new_change] = getChange(cost, your_bills)
        if oh_no == 1:
            #tell user they haven't inserted enough money and threaten them
            #Workers of the world, unite!01e3p
            print('Insufficient Funds')
            print('Not trying to fool ze motherland are we comrade?')
            print('Insert more money and remember...')
            print('Big brother is watching')
            drink = 0
            pushed_key = None
            state = 2
            
        else:
            print('Dispensing Change: {} '.format (new_change))
            print('And your {} comrade'.format (drink))
            drink = 0
            state = 0
    
    else:
        """this state shouldn't exist!
        """
        pass
