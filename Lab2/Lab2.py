# -*- coding: utf-8 -*-
"""@file Lab2.py
@brief Reaction Time Game
@details Program initializes and prompts you to play a game in which
         you will attempt to press the blue button on our microcontroller
         as soon as you see the green light turn on. Once you press it, 
         the green light will turn off and your reaction time will be 
         presented in microseconds, and it will wait a random time period
         between 0 and 6 seconds to turn on again. If you do not press the
         green light within 1 second, it will automatically exit and the game 
         will be over. When the user presses control-c, their average time
         will be presented in seconds. (That is how I understood this 
         assignment.)
@author Jacob Everest
@date Jan. 21, 2021
@copyright I still don't know what that word means
"""
import pyb                      #import pyb library
import random                   #import random library
##@brief set led as blinky
# @details When I refer to blinky, I am refering to the LED pin on the board
blinky = pyb.Pin(pyb.Pin.board.PA5, mode = pyb.Pin.OUT_PP)
blinky.low()                    #set blinky to be low upon startup
##@brief Blinky Flag
# @details Always set as either 0 if blinky is off or 1 if blinky is on
#          this will be used to let the interrupt know whether the button
#          has been pressed appropriately or not.
blinky_status = 0
##@brief Sum of the past reaction times
# @details adds up the reaction times to compute the average later
reaction_sum = 0
##@brief Number of times played
# @details Number of reaction times by the user, used to compute the average
reaction_num = 0
##@brief Time on the counter
# @details Stores the time on the counter when the user interrupted
counter_status = 0
##@brief A random time
# @details Used to store a random time between 0 and 6 seconds for the 
#          LED to turn on
random_time = 0
##@brief Value of Average
# @details Average response of the user after exiting
average_response = 0
##@brief Value of the state
# @details I still have 305 assembly language PTSD. I can't not use states
state = 0
random_time = random.randint(500000, 6000000) #create a random time interval
#for the first time when the LED turns on
tim = pyb.Timer(2, prescaler=79, period = 0x7fffffff) #set timer 2 to 
#count up until 0x7fffffff, and stretch out T to be 1 microsecond

def count_isr (PC13):        # Create an interrupt service routine
    """ 
    @brief Button Push ISR
    @details When the user presses the PC13 button, the program will jump here.
             This ISR will determine if the LED was on. If so it will record
             the user reaction time, display that, add that time to the sum of
             their reaction times, and increment the number of times they have
             played.
    @param PC13 the value of the pin to be pushed on the microcontroller
    @author Jacob Everest
    @date Jan. 21, 2021
    """
    global reaction_sum      #make it global so it can be used in our ISR
    global reaction_num      #^
    global random_time       #^
    global blinky_status     #^
    global state             #^
    global average_response  #^
    counter_status = tim.counter() #at the start save the counter value upon ISR
    if blinky_status == 1:   #if blinky LED is actually on
        reaction_sum = counter_status + reaction_sum #add the counter to the sum
        reaction_num = reaction_num + 1 #increment the number of rounds played
        blinky.low()         #turn off blinky because button pushed
        blinky_status = 0    #when blinky is turned off, blinky status is zero
        tim.counter(0)       #reset the timer counter
        state = 0            #state is now zero
        random_time = random.randint(500000, 6000000) #get a new random time
        #for our LED
        print(counter_status) #print what the user's reaction time was
    else:                    #if LED wasn't on, this isn't the ISR you're
                             #looking for
        pass

extint = pyb.ExtInt (pyb.Pin.board.PC13, #enable the pin specified to trigger
                  pyb.ExtInt.IRQ_FALLING,#the interrupt externally
                  pyb.Pin.PULL_UP, 
                  count_isr)    

#Print the following welcome message
print('When the light turns on you have 1 second to press the blue button.')
print('If the light turns off before you press the button, the game will end')
print('The goal is to press the blue button as quickly as possible once the')
print('the light turns on. Your reaction time will be recorded and your')
print('average will be given at the end. Press Ctrl-C to exit and see')
print('your time.')
print('Press Enter to begin')
#Just wait here until the user presses enter
input()

tim.counter(0) #reset the timer counter to zero again

while True:
    try:
        if state == 1: #if blinky is now on
            while state == 1: #while blinky is on
                if tim.counter() < 1000000: #as long as it's been less than 
                    pass                    #one second
                else:
                    blinky.low() #turn off blinky
                    state = 2    #go to state 2 (oh no! User waited too long!)
        if state == 2:           #the dreaded state two!
            break                #YOU SHALL NOT PASS
        
        else:           #if blinky is now off
            #I'm actually quite proud of the code below. I created the random 
            #time variable so that the light will be triggered between 0.5 and
            #6 seconds after the last light was turned off. However, I couldn't 
            #do an if equals because it was rare that the code would be here 
            #when the timer was equal to the random time. Instead, I gave it a 
            #range around the random time so that the random timing is 
            #conserved but this code will catch the trigger within one second.
            #it's like a backstop for my code
            if random_time - 500000 <= tim.counter() <= random_time + 500000:
                tim.counter(0)   #set timer back to zero to count the response
                blinky.high()    #turn on blinky
                blinky_status = 1#always set blinky status to one when turned on
                state = 1        #Send it back to state one on next pass
            else:
                pass             #Just skip if waiting for the time to be right
    except KeyboardInterrupt:
        break                    #exit
    
if reaction_num == 0:            #If the button was never pressed, let them know
    print('Error: you did not press anything and time ran out.')
else:
    average_response = (reaction_sum/reaction_num)/1000000 #average time calc
    print ('Average Response Time Was {}s'.format (average_response))   

    