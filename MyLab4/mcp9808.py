# -*- coding: utf-8 -*-
"""
Created on Thu Feb  4 10:19:12 2021

@author: jacob
"""
import pyb
from pyb import I2C


class mcp9808:
    """
    @brief Reads MCP9808 Data
    @details Input a premade object and the address of the I2C device to 
             read from. Call this class and the method of either check,
             celsius, or fahrenheit. Check will return the address of the 
             device, celsius will return the temperature in degrees celsius,
             and fahrenheit will return the temperature in degrees fahrenheit. 
    @author Jacob Everest
    @author Hunter Morse
    @date Feb. 2, 2021
    @copyright I am now refusing to learn what this word means
    """
    def __init__(self, object_name, address):
        """ 
        @brief Initialization Function
        @details Stores the parameters input by the user when creating the 
                 class object so that they may be used later by other 
                 functions.
        @param object_name the name of an I2C object already created by the user
        @param address the location of the I2C device on the nucleo
        @author Hunter Morse
        @author Jacob Everest
        @date Feb. 2, 2021
        """
        self.object_name = object_name
        self.address = address
    
    def check(self):
        """ 
        @brief Checks the Location
        @details Checks the location of the I2C device on the nucleo
        @param None
        @author Hunter Morse
        @author Jacob Everest
        @date Feb. 2, 2021
        """
        return self.object_name.scan()
    
    def celsius(self):
        """ 
        @brief Measures the temperature in celsius
        @details Reads the data from the MCP9808 in the form of two bytes. It
                 Takes the important sign information from the the bit five of 
                 the first bit, then gets rid of the four MSBs in that bit. It
                 then bit shifts to the left four times on that same byte. It 
                 bit shifts to the right four times on the second byte, then
                 combines the two into one. Then it checks to see if the sign
                 bit was one, thereby declaring the reading to be negative. If
                 it was, then it subratracts the value from 256, if not it does
                 nothing and returns that value as thetemperature.
        @param None
        @author Hunter Morse
        @author Jacob Everest
        @date Feb. 2, 2021
        """
        buf = bytearray([0, 0]) #creating an editable buffer for bytes from mcp9808
        self.object_name.mem_read(buf, self.address, 5)
        jack = 0
        if (buf[0] & 0b00010000):
            jack = 1
        else:
            pass
        buf[0] = buf[0] & 0b00001111
        shoot = 16
        buf[0] = buf[0]*shoot
        if jack == 1:
            temp = -(256 - (buf[0] +buf[1]/shoot))
        else:
            temp = buf[0]+buf[1]/shoot
        return temp
        
    def fahrenheit(self):
        """ 
        @brief Measures the temperature in celsius
        @details Reads the data from the MCP9808 in the form of two bytes. It
                 Takes the important sign information from the the bit five of 
                 the first bit, then gets rid of the four MSBs in that bit. It
                 then bit shifts to the left four times on that same byte. It 
                 bit shifts to the right four times on the second byte, then
                 combines the two into one. Then it checks to see if the sign
                 bit was one, thereby declaring the reading to be negative. If
                 it was, then it subratracts the value from 256, if not it does
                 nothing. It converts from celsius to fahrenheit, then it
                 returns that value as thetemperature.
        @param None
        @author Hunter Morse
        @author Jacob Everest
        @date Feb. 2, 2021
        """
        # buf = bytearray([0, 0])
        # self.object_name.mem_read(buf, self.address, 5)
        # jack = 0
        # if (buf[0] & 0b0001000):
        #     jack = 1
        # else:
        #     pass
        # buf[0] = buf[0] & 0b00001111
        # shoot = 16
        # buf[0] = buf[0]*shoot
        # if jack == 1:
        #     temp = 256 - (buf[0] +buf[1]/shoot)
        # else:
        #     temp = buf[0]+buf[1]/shoot
        temp = (self.celsius()*9/5)+32
        return temp
        
    

